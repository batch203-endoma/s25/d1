// [SECTION] JSON Methods
// The "JSON Object" contains methods for parsing and converting data into stringified JSON.
// JSON data is sent or received in text-only (String) format.

// Converting data into Stringified JSON


//javaScript array of Objects

let batchesArr = [
  {
    batchName: "Batch 203",
    schedule: "Full Time"

  },
  {
    batchName:"Batch 204",
    schedule:"Part Time"

  }
]

console.log(batchesArr);

// The strinify Method:
//JavaScript Objects into a string.
console.log("Result from stringify method: ");
console.log(JSON.stringify(batchesArr));

let data =JSON.stringify({
  name:"John",
   age:31,
   address:{
     city:"Manila",
     country:"Philippines"
   }
})

console.log(data);

//User Details

// let firstName = prompt("Enter your first name: ");
// let lastName = prompt(`Enter your last name: `);
// let email = prompt(`Enter your email`);
// let password = prompt(`Enter your password`);
//
// let otherData =JSON.stringify({
//   firstName: firstName,
//   lastName: lastName,
//   email: email,3
//   password: password
// })
//
// console.log(otherData);

// [SECTION] Converting stringified JSON into JavaScript Objects
let batchesJSON = `[
  {
    "batchName": "Batch 203",
    "schedule": "Full Time"

  },
  {
    "batchName":"Batch 204",
    "schedule":"Part Time"

  }
]`

console.log(`batchesJSON content: `);
console.log(batchesJSON);


//JSON.parse method to convert JSON Object into JavaScript Objects
console.log("Result from parse method: ");
// console.log(JSON.parse(batchesArrJSON));
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches[0].batchName);

let stringifiedObject = `{
  name:"John",
   age:31,
   address:{
     city:"Manila",
     country:"Philippines"
   }
})`


console.log(parseBatches);
